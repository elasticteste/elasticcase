<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<div class="card" style="border: 1px solid black; background-color: lightcyan; text-align: center;" >
	<h1>Obrigado Por Usar Nosso Serviço</h1>
	<h1>Status da entrega:</h1>
	<div id="div" style="font-size: 20px"></div>
</div>
<?php
	include 'config.php';
	include 'script.php';
	
	include PATH_DAO. 'FormatPdf.php';
	include PATH_DAO. 'SendMail.php';
	$sendMail = new SendMail();
	$pdf = new FormatPdf();
	//Código do Objeto
	$obj = 'OA016913717BR';
	
	$post = array('Objetos' =>  $obj );	
	
	//inicia o CURL
	$ch = curl_init();

	//configurações de consumo da api externa
	curl_setopt($ch, CURLOPT_URL, "https://www2.correios.com.br/sistemas/rastreamento/resultado_semcontent.cfm");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
	
	//execução
	$res = curl_exec($ch);

	//script para converter em pdf
	$pdf->FormatPdf($res);
	
	//resultado impresso na tela
	echo utf8_encode($res);	

	//fecha o curl
	curl_close($ch);
	
	
	//se deu tudo certo, ele envia o email para o cliente
	if($res){
		$sendMail->SendMail($res);
	}else{
		return;
	}
?>
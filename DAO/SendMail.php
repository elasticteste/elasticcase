<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<link rel="stylesheet" href="sweetalert2/dist/sweetalert2.min.css">

<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\Exception;

    require PATH_LIB. 'PHPMailer/src/Exception.php';
	require PATH_LIB. 'PHPMailer/src/PHPMailer.php';
    require PATH_LIB. 'PHPMailer/src/SMTP.php';

class SendMail{

    function SendMail($res){

		//configurações do email
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->CharSet = 'UTF-8';
		$mail->Host = 'smtp.gmail.com';
		$mail->SMTPAuth = true;
		$mail->SMTPSecure = 'tls';
		$mail->Username = 'vinicius.s.mario@gmail.com';
		$mail->Password = '28091999';
		$mail->Port = 587;
				
		$mail->setFrom('vinicius.s.mario@gmail.com');
		$mail->addReplyTo('no-reply@email.com.br');
		$mail->addAddress('vinicius-mario@hotmail.com', 'Nome');
		$mail->addAddress('vinicius.s.mario@gmail.com', 'Contato');
		$mail->isHTML(true);
		$mail->Subject = 'Encomenda';
		$mail->Body  = utf8_encode($res);
		$mail->Body .= 'Dados de envio: <br>';
		$mail->Body.= 'Vinícius Soares Mário <br>';
		$mail->Body .= 'Cel: (19) 9 9907-0337 <br>';
		$mail->Body .= 'Cidade: São José do Rio Pardo <br>';	
		$mail->AddAttachment('C:/Users/Vinicius Mário/Downloads/encomenda'.date('d-m-Y').'.pdf');
		
		if($mail->send()){
			?>
			<script>
			Swal.fire({
			  position: 'center',
			  icon: 'success',
			  title: 'E-mail enviado com sucesso!!!',
			  showConfirmButton: false,
			  timer: 1500
			})
			</script>
		<?php
		}
		else{
			?>
			<script>
			Swal.fire({
			  position: 'center',
			  icon: 'error',
			  title: 'Não foi possível enviar o email!!!',
			  showConfirmButton: false,
			  timer: 1500
			})
			</script>
		<?php
		}

    }
}

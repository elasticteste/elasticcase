<?php 
    require_once PATH_LIB. '/dompdf/autoload.inc.php';
    use Dompdf\Dompdf;

class FormatPdf{

    function FormatPdf($res){
        $dompdf = new Dompdf();
        
        $dompdf->loadHtml(utf8_encode($res));
        
        $dompdf->setPaper('A4', 'portrait');
        
        $dompdf->render();
        
        $output = $dompdf->output();
        
        file_put_contents('C:/Users/Vinicius Mário/Downloads/encomenda'.date('d-m-Y').'.pdf', $output);	
    }
}
?>